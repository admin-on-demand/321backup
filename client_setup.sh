#!/usr/bin/env bash

yaml_parse() {
  local file=$1
  local prefix=$2
  local s='[[:space:]]*'
  local w='[a-zA-Z0-9_]*'
  local fs=$(echo @|tr @ '\034')

  sed -ne "s|^\($s\):|\1|" \
      -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
      -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p" ${file} |
  awk -F$fs '{
    indent = length($1)/2;
    vname[indent] = $2;
    for (i in vname) {if (i > indent) {delete vname[i]}}
    if (length($3) > 0) {
       vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
       printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
    }
  }'
  return 0
}

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

eval $(yaml_parse ${SCRIPT_DIR}/config.yml)

sudo adduser --disabled-password ${two_user}

sudo mkdir -v /home/${two_user}/.ssh

sudo sh -c "cat ${SCRIPT_DIR}/pubkeys/* >> /home/${two_user}/.ssh/authorized_keys"

sudo chmod 640 /home/${two_user}/.ssh/authorized_keys
sudo chown -R ${two_user}:${two_user} /home/${two_user}/.ssh/

sudo sh -c "echo '%${two_user} ALL=(ALL) NOPASSWD: /usr/bin/rsync' >> /etc/sudoers.d/${two_user}"
